import argparse
import datetime
import datetime as dt
import decimal
import json
import multiprocessing as mp
import os
from datetime import date
from json import JSONEncoder
from random import randint

import numpy
import pandas as pd
import dateutil.relativedelta
from pandas.tseries.holiday import (
    AbstractHolidayCalendar, DateOffset, EasterMonday,
    GoodFriday, Holiday, MO,
    next_monday, next_monday_or_tuesday)

env_name = 'demo'
url_name = 'demo'

base_path = os.path.join("/Users", "data", "test_data")
current_txn_list = []
firstyear_txn_list = []
# secondyear_txn_list = []
# thirdyear_txn_list = []
# fourthyear_txn_list = []

total_no_transaction = 0
count = 0


def random_N(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def transaction_payload(provider, date, trnx_type, amount):
    transactioncode = "ReceivedTransaction"
    global total_no_transaction
    total_no_transaction = total_no_transaction + 1
    if (trnx_type == 'D/D'):
        transactioncode = "ReceivedDirectDebits"
    if (trnx_type == 'S/O'):
        transactioncode = "ReceivedStandingOrders"
    if (trnx_type == 'POS'):
        transactioncode = "ReceivedPOS"
    if (trnx_type == 'BAC'):
        transactioncode = "ReceivedBACSCREDIT"
    if (trnx_type == 'CHQ'):
        transactioncode = "ReceivedCHEQUE"
    if (trnx_type == 'CWL'):
        transactioncode = "ReceivedCashWithDrawl"
    if (trnx_type == 'DPC'):
        transactioncode = "ReceivedDigitalBankingPayment"
    txn = {
        "id": random_N(8),
        "ownedItemId": random_N(6),
        "reference": str(random_N(5)),
        "createdDate": date,
        "bookingDate": date,
        "valueDate": date,
        "amount": amount,
        "transactionId": random_N(8),
        "currency": "GBP",
        "balance": 100.00,
        "status": "booked",
        "merchant": {
            "name": "provider",
            "isoCode": "5411"
        },
        "address": {
            "line1": "40 Islington Street",
            "city": "LONDON",
            "country": "UK",
            "postcode": "N18XB",
            "latitude": 1234.00,
            "longitude": 1234.00
        },
        "codes": [
            {
                "scheme": "OpenBanking",
                "code": transactioncode,
                "subCode": "Interests(Generic)"
            },
            {
                "scheme": provider,
                "code": trnx_type,
            }
        ],
        "categorisations": [],
        "descriptions": [
            {
                "scheme": "OpenBanking",
                "text": provider
            }
        ]
    }
    return txn


def pending_transaction_payload(provider, date, trnx_type, amount):
    transactioncode = "PendingTransaction"
    global total_no_transaction
    total_no_transaction = total_no_transaction + 1
    if (trnx_type == 'D/D'):
        transactioncode = "PendingDirectDebits"
    if (trnx_type == 'S/O'):
        transactioncode = "PendingStandingOrders"
    if (trnx_type == 'POS'):
        transactioncode = "PendingPOS"
    if (trnx_type == 'BAC'):
        transactioncode = "PendingBACSCREDIT"
    if (trnx_type == 'CHQ'):
        transactioncode = "PendingCHEQUE"
    if (trnx_type == 'CWL'):
        transactioncode = "PendingCashWithDrawl"
    if (trnx_type == 'DPC'):
        transactioncode = "PendingDigitalBankingPayment"
    txn = {
        "id": random_N(10),
        "ownedItemId": random_N(6),
        "createdDate": date,
        "bookingDate": date,
        "amount": amount,
        "currency": "GBP",
        "status": "Pending",
        "codes": [
            {
                "code": "D/D"
            }
        ],
        "categorisations": [],
        "descriptions": [
            {
                "scheme": "OpenBanking",
                "text": provider
            }
        ]
    }
    return txn


class EnglandAndWalesHolidayCalendar(AbstractHolidayCalendar):
    rules = [
        Holiday('New Years Day', month=1, day=1, observance=next_monday),
        GoodFriday,
        EasterMonday,
        Holiday('Early May bank holiday',
                month=5, day=1, offset=DateOffset(weekday=MO(1))),
        Holiday('Spring bank holiday',
                month=5, day=31, offset=DateOffset(weekday=MO(-1))),
        Holiday('Summer bank holiday',
                month=8, day=31, offset=DateOffset(weekday=MO(-1))),
        Holiday('Christmas Day', month=12, day=25, observance=next_monday),
        Holiday('Boxing Day',
                month=12, day=26, observance=next_monday_or_tuesday)
    ]


def uk_holiday(year):
    holidays = EnglandAndWalesHolidayCalendar().holidays(
        start=date(year, 1, 1),
        end=date(year, 12, 31))
    #     holidays2=holidays.tolist()
    #     holidays3=holidays.to_pydatetime()
    list_holiday = holidays.to_native_types()
    holiday_list = []
    for holiday in list_holiday:
        formatHoliday = datetime.datetime.strptime(holiday, "%Y-%m-%d").date()
        holiday_list.append(formatHoliday)
    return holiday_list


def weekend(date):
    date = pd.to_datetime(date)
    dow = dt.datetime.weekday(date)
    if dow in (5, 6):
        delta = 7 - dow
        date = date + dateutil.relativedelta.relativedelta(days=delta)
    return date


def bank_hol(date):
    year = date.year
    month = date.month
    dom = (date + numpy.timedelta64(0, 'D')).day
    convertdate = str(year) + '-' + str(month) + '-' + str(dom)
    dateCheck = datetime.datetime.strptime(convertdate, "%Y-%m-%d").date()
    list_uk_holidays = uk_holiday(year)
    if dateCheck in list_uk_holidays:
        dateCheck = pd.to_datetime(dateCheck) + numpy.timedelta64(1, 'D')
    return dateCheck


def make_weekday(date):
    while (1 > 0):
        date_new = weekend(bank_hol(date))
        date = weekend(bank_hol(date_new))
        if (date_new == date):
            break
    return date_new


def historic_dates(surfaceDay, noOfTrans):
    start = pd.to_datetime('today') + numpy.timedelta64(surfaceDay, 'D')
    date_of_month = start.day
    start_point = start + pd.DateOffset(months=(-1) * noOfTrans, day=date_of_month)
    dates = pd.date_range(start=start_point, periods=noOfTrans,
                          freq=pd.DateOffset(months=1, day=date_of_month)).to_series().reset_index(drop=True)
    #     current_time = datetime.datetime.utcnow().strftime("%H:%M:%S+00:00")
    current_time = "00:00:00+00:00"
    return dates.apply(lambda x: make_weekday(x).strftime(f"%Y-%m-%dT{current_time}"))


def calculate_dates(startPoint, noOfTrans):
    start = pd.to_datetime('today') + numpy.timedelta64(startPoint, 'D')
    dates = pd.date_range(start, periods=noOfTrans, freq='D').to_series().reset_index(drop=True)
    #     current_time = datetime.datetime.utcnow().strftime("%H:%M:%S+00:00")
    current_time = "00:00:00+00:00"
    return dates.apply(lambda x: make_weekday(x).strftime(f"%Y-%m-%dT{current_time}"))


def create_regular_transaction(merchantlist, currentDate, historicDate, trnx_type, amount):
    counter = 0
    for currDate in currentDate:
        transaction = transaction_payload(merchantlist, currDate, trnx_type, amount)
        current_txn_list.append(transaction)
    for hDate in historicDate:
        transaction = transaction_payload(merchantlist, hDate, trnx_type, amount)
        if (counter < 13):
            firstyear_txn_list.append(transaction)
        # if (13 <= counter < 25):
        #     secondyear_txn_list.append(transaction)
        # if (25 <= counter < 37):
        #     thirdyear_txn_list.append(transaction)
        # if (37 <= counter < 49):
        #     fourthyear_txn_list.append(transaction)
        counter = counter + 1


def create_all_transaction(providers, trnx_type, amount, options, multi=True):
    i = 0
    N = len(providers)
    all_res = list()
    global total_no_transaction
    for i, provider in zip(range(N), providers):
        currentDate = calculate_dates(-81, 81)
        historicDate = historic_dates(-i, 13)

        if multi:
            print("Using multiprocessing transaction created : " + str(total_no_transaction))
            with mp.Pool(processes=options.ncores) as pool:
                p = pool.apply_async(
                    create_regular_transaction(provider, currentDate, historicDate, trnx_type, amount))
                all_res.append(p)


def generate_pending_dd_stories():
    trnx_type = "D/D"
    date_range = calculate_dates(0, 1)
    amount = -4
    merchantlist = ['HALIFAX', 'CAMELOT LOTTERY']
    for merchant in merchantlist:
        for date in date_range:
            transaction = pending_transaction_payload(merchant, date, trnx_type, amount)
            current_txn_list.append(transaction)


def generate_pending_so():
    trnx_type = "S/O"
    date_range = calculate_dates(0, 1)
    amount = -4
    merchantlist = ['VAN INS', 'CABOT']
    for merchant in merchantlist:
        for date in date_range:
            transaction = pending_transaction_payload(merchant, date, trnx_type, amount)
            current_txn_list.append(transaction)


def generate_benefit_received_transaction():
    trnx_type = "BAC"
    date_range = historic_dates(30, 12)
    amount = 42
    providers = ['WORK AND CHILD TC','WORKING TAX CREDIT',
                        'DWP EESA', 'DWPCMS', 'DWP IS','DWP JSA']
                        # 'DWP JSA','DWP PIP', 'DWP UC', 'DWP SF', 'DWP WB',
                        # 'DWP CA', 'DWP BB', 'DWP MA', 'DWP XB'
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 12):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1

def generate_cheque_Left_transaction():
    trnx_type = "CHQ"
    date_range = calculate_dates(-1, 3)
    amount = -30
    merchantlist = ['CHQ']
    for merchant in merchantlist:
        for date in date_range:
            transaction = transaction_payload(merchant, date, trnx_type, amount)
            current_txn_list.append(transaction)


def generate_pending_cheque_Left_transaction():
    trnx_type = "CHQ"
    date_range = calculate_dates(0, 1)
    amount = -20
    merchantlist = ['CHQ']
    for merchant in merchantlist:
        for date in date_range:
            transaction = pending_transaction_payload(merchant, date, trnx_type, amount)
            current_txn_list.append(transaction)


def generate_cheque_arrived_transaction():
    trnx_type = "CHQ"
    date_range = calculate_dates(-1, 4)
    amount = 42
    merchantlist = ['CHQ']
    for merchant in merchantlist:
        for date in date_range:
            transaction = transaction_payload(merchant, date, trnx_type, amount)
            current_txn_list.append(transaction)


def generate_pending_cheque_arrived_transaction():
    trnx_type = "CHQ"
    date_range = calculate_dates(0, 1)
    amount = 22
    merchantlist = ['CHQ']
    for merchant in merchantlist:
        for date in date_range:
            transaction = pending_transaction_payload(merchant, date, trnx_type, amount)
            current_txn_list.append(transaction)


def generate_pending_pos_transaction():
    trnxtype = "POS"
    amount = -45
    providers = ['COSTA', 'EAT LTD231', 'NEWDAY LIMITED', 'NEXT LEVEL BAR', 'ODEON KINGSWEST', '32 RED PLC',
                 'MONSOON ACCZ LTD', 'NATIONWIDE B S']

    # providers = ['COSTA']

    date_range = calculate_dates(0, 1)
    for merchant in providers:
        amount = amount + 1
        for date in date_range:
            transaction = pending_transaction_payload(merchant, date, trnxtype, amount)
            current_txn_list.append(transaction)


def generate_refund_transaction():
    trnxtype = "POS"
    amount = 45
    providers = ['REFUND']
    # , 'REVERSAL', 'CHARGEBACK', '3940 31 MAY19 NORTON XAP121692584 201616500GB REFUND',
    #              '5571 08NOV15      GI LTD            WOKING GB         REFUND',
    #              '4707 28JUN18      AMZ*MINASAN       800-279-6620 LU   REFUND',
    #              '5249 19MAR16      PRIMARK 673       LEEDS GB          REFUND',
    #              '4414 31DEC17      KFC-SITTINGBOURNE SITTINGBOURNE GB  REFUND',
    #              '3553 27FEB18      GI LTD            WOKING GB         REFUND']
    # providers = ['4707 28JUN18      AMZ*MINASAN       800-279-6620 LU   REFUND']
    date_range = calculate_dates(-1, 1)
    for merchant in providers:
        amount = amount + 1
        for date in date_range:
            transaction = transaction_payload(merchant, date, trnxtype, amount)
            current_txn_list.append(transaction)


def generate_pending_refund():
    trnxtype = "POS"
    amount = 15
    providers = ['5571 08NOV15      GI LTD            WOKING GB         REFUND']
    date_range = calculate_dates(0, 1)
    for merchant in providers:
        amount = amount + 1
        for date in date_range:
            transaction = transaction_payload(merchant, date, trnxtype, amount)
            current_txn_list.append(transaction)

def generate_anniversary_transaction():
    trnx_type = "D/D"
    amount = -200
    date_range = historic_dates(30, 12)
    providers = ['ORANGE HOME','MINI CAR INSURANCE','ZOPA LIMITED','HOMESERV UTD UTILS']
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 12):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1


def generate_salary_transaction():
    trnx_type = "BAC"
    amount = 1800
    date_range = historic_dates(0, 12)
    providers = ['NATWEST HRPS PAYRO']
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 1):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1


def generate_all_pending_transaction():
    generate_pending_dd_stories()
    generate_pending_cheque_Left_transaction()
    generate_pending_cheque_arrived_transaction()
    generate_pending_pos_transaction()
    generate_pending_so()
    generate_pending_refund()


def generate_regulardd_transaction(options, multi=True):

    # providers = ['ACCORD MORTGAGES','RBS PERSONAL LOAN','HME RTL GRP CARDS','HALIFAX INS IN-HOU']
    providers = ['DVLA-AP52WOD','TV LICENCE MBP','HLAM REGULAR SAVIN','BIRMINGHAM CITY CO']
    trnx_type = "D/D"
    amount = -42
    date_range = historic_dates(30, 13)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 13):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1
    # create_all_transaction(providers, trnx_type, amount, options, multi=True)


def generate_regularso_transaction(options, multi=True):
    # providers = ['CHARITY', 'LLOYDS TSB', 'CREDIT CRD', 'UNITED UTILITIES', 'BONUS BALL', 'C-TAX', 'BARNARDOS',
    #              'SATSUMA LOAN', 'CAPITAL ONE']
    # ,,'LLOYDS TSB'
    providers = ['BONUS BALL','CHARITY']
#     , 'LLOYDS TSB', 'CREDIT CRD', 'UNITED UTILITIES', 'BONUS BALL', 'C-TAX', 'BARNARDOS','SATSUMA LOAN', 'CAPITAL ONE','EON ENERG','COUNCIL TAX','MORTGAGES','MASTERCARD','CREDIT CARD','CREDIT CAR','NSPCC',
# 'RSPCA','CABOT','MOORCROFT','LOWELL','REWARD RESERVE','BARCLAY CC','ADVANTAGE RESERVE','BARCLAYS BANK','ABBEY NATIONAL',
# 'ROYAL BANK OF SCOT','NATIONWIDE BS']
    trnx_type = "S/O"
    amount = -22
    date_range = historic_dates(30, 13)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 13):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1

def generate_upcomingdd_transaction(options, multi=True):

    providers = ['NURSING AND MIDWIF','TSB PLC','EAST SURREY WATER','AMNESTY INT UK','CHURCHILL INSURANC']

    trnx_type = "D/D"
    amount = -36
    date_range = historic_dates(7, 12)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 1):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1

def generate_upcomingso_transaction(options, multi=True):
    # providers = ['CHARITY', 'LLOYDS TSB', 'CREDIT CRD', 'UNITED UTILITIES', 'BONUS BALL', 'C-TAX', 'BARNARDOS',
    #              'SATSUMA LOAN', 'CAPITAL ONE']
    providers = ['NATIONWIDE BS','ROYAL BANK OF SCOT']
#     , 'LLOYDS TSB', 'CREDIT CRD', 'UNITED UTILITIES', 'BONUS BALL', 'C-TAX', 'BARNARDOS','SATSUMA LOAN', 'CAPITAL ONE','EON ENERG','COUNCIL TAX','MORTGAGES','MASTERCARD','CREDIT CARD','CREDIT CAR','NSPCC',
# 'RSPCA','CABOT','MOORCROFT','LOWELL','REWARD RESERVE','BARCLAY CC','ADVANTAGE RESERVE','BARCLAYS BANK','ABBEY NATIONAL',
# 'ROYAL BANK OF SCOT','NATIONWIDE BS']
    trnx_type = "S/O"
    amount = -45
    date_range = historic_dates(7, 6)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 1):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1

def generate_current_pos_transaction(options, multi=True):
    providers = ['COSTA', 'EAT LTD231', 'NEWDAY LIMITED', 'NEXT LEVEL BAR', 'ODEON KINGSWEST', '32 RED PLC',
                 'MONSOON ACCZ LTD', 'NATIONWIDE B S']
    trnxtype = "POS"
    amount = -2.10
    date_range = calculate_dates(-81, 81)
    for merchant in providers:
        amount = amount + 1
        for date in date_range:
            transaction = transaction_payload(merchant, date, trnxtype, amount)
            firstyear_txn_list.append(transaction)

def generate_historic_pos_transaction(options, multi=True):
    providers = ['COSTA', 'EAT LTD231', 'NEWDAY LIMITED', 'NEXT LEVEL BAR', 'ODEON KINGSWEST', '32 RED PLC',
                 'MONSOON ACCZ LTD', 'NATIONWIDE B S']
    trnx_type = "POS"
    amount = -2.10
    date_range = historic_dates(0, 13)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 13):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1


    # historicDate = historic_dates(-i, 13)
    # create_all_transaction(providers, trnx_type, amount, options, multi=True)

def generate_foreign_travel_checklist_transaction(options, multi=True):
    providers = ['8127 20SEP19 C    BREWERS FAYRE     HEATHSIDE GB']
    # provider = ["8127 20SEP19 C    AIR FRANCE        KEMPTON PARK ZA"]
    #
    # provider = ["8127 20SEP19 C    BREWERS FAYRE     HEATHSIDE GB"]
    #
    #  provider = ["8127 20SEP19 C    COSTA ATLANTICA   GENOA IT"
    trnx_type = "POS"
    amount = -250.00
    date_range = historic_dates(30, 11)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 1):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1

def generate_car_servicing_transaction(options, multi=True):
    providers = ['5135 28JAN20 C    HUSSEY AUTOS      WOKINGHAM GB']
    trnx_type = "POS"
    amount = -200.00
    date_range = historic_dates(30, 11)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 1):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1


def generate_historic_cwl_transaction(options, multi=True):
    providers = ['Cashwithdrawl1', 'Cashwithdrawl2']
    trnx_type = "C/L"
    amount = -112
    date_range = historic_dates(0, 13)
    for provider in providers:
        count = 1
        for date in date_range:
            transaction = transaction_payload(provider, date, trnx_type, amount)
            if (count == 13):
                current_txn_list.append(transaction)
            else:
                firstyear_txn_list.append(transaction)
            count = count + 1

def generate_monthly_cwl_transaction(options, multi=True):
    providers = ['Cashwithdrawl1', 'Cashwithdrawl2']
    trnx_type = "C/L"
    amount = -112
    date_range = calculate_dates(-39, 39)
    for merchant in providers:
        amount = amount + 1
        for date in date_range:
            transaction = transaction_payload(merchant, date, trnx_type, amount)
            firstyear_txn_list.append(transaction)

def generate_historic_internal_transfer_credit(options, multi=True):
    providers = ['TO A/C 77773333VIA MOBILE - XFER']
    # , 'TO A/C 77773333VIA ONLINE - XFER',
    #              '', 'noninternalcrebit']
    trnx_type = "DPC"
    amount = 20
    i = 0
    N = len(providers)
    all_res = list()
    global total_no_transaction
    for i, provider in zip(range(N), providers):
        currentDate = calculate_dates(-46, 46)
        historicDate = historic_dates(-i, 13)

        if multi:
            print("Using multiprocessing transaction created : " + str(total_no_transaction))
            with mp.Pool(processes=options.ncores) as pool:
                p = pool.apply_async(
                    create_regular_transaction(provider, currentDate, historicDate, trnx_type, amount))
                all_res.append(p)

def generate_historic_internal_transfer_debit(options, multi=True):
    providers = ['noninternaldebit']
    # , 'FROM A/C 77773333VIA ONLINE - XFER',
    #              'FROM A/C 77773333VIA MOBILE - XFER', '']
    trnx_type = "DPC"
    amount = 20
    i = 0
    N = len(providers)
    all_res = list()
    global total_no_transaction
    for i, provider in zip(range(N), providers):
        currentDate = calculate_dates(-45, 45)
        historicDate = historic_dates(-i, 13)

        if multi:
            print("Using multiprocessing transaction created : " + str(total_no_transaction))
            with mp.Pool(processes=options.ncores) as pool:
                p = pool.apply_async(
                    create_regular_transaction(provider, currentDate, historicDate, trnx_type, amount))
                all_res.append(p)


def write_file():
    filenameList = {"RecentTransactions_Batch3.json": current_txn_list,
                    "History_Transactions_Batch3.json": firstyear_txn_list}
    for filename, translist in filenameList.items():
        with open(os.path.join(base_path, filename), 'w') as f:
            json.dump(translist, fp=f)


# @pytest.fixture(scope='session')
def generate_all_transaction():
    #Delta
    # generate_dd_stories()
    generate_benefit_received_transaction()
    generate_cheque_Left_transaction()
    generate_cheque_arrived_transaction()
    generate_refund_transaction()
    generate_all_pending_transaction()



# history transactions
    generate_anniversary_transaction()
    generate_salary_transaction()

    generate_current_pos_transaction(options, multi=True)
    generate_historic_pos_transaction(options, multi=True)
    generate_historic_cwl_transaction(options, multi=True)
    generate_monthly_cwl_transaction(options, multi=True)
    generate_regulardd_transaction(options, multi=True)
    generate_regularso_transaction(options, multi=True)
    generate_upcomingdd_transaction(options, multi=True)
    generate_upcomingso_transaction(options, multi=True)
    generate_historic_internal_transfer_credit(options, multi=True)
    generate_historic_internal_transfer_debit(options, multi=True)
    generate_car_servicing_transaction(options, multi=True)
    generate_foreign_travel_checklist_transaction(options, multi=True)




if __name__ == '__main__':

    time_start = datetime.datetime.now()
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--create", default=False, action='store_true')
    parser.add_argument("-m", "--multi", default=True, action='store_true')
    parser.add_argument("-n", "--ncores", default=6, type=int)
    options = parser.parse_args()
    if options.create:
        # create_transaction(options, options.multi)
        generate_all_transaction()
    # print(current_txn_list)
    write_file()
    print("Total Number of transaction created : " + str(total_no_transaction))
